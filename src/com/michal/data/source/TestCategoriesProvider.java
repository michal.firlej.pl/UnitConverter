package com.michal.data.source;

import com.michal.data.Category;
import com.michal.data.Unit;

import java.util.ArrayList;
import java.util.List;

public class TestCategoriesProvider implements CategoriesSource {
    public List<Category> get() {
        List<Category> categories = new ArrayList<>();

        categories.add(new Category("Test").addUnit(new Unit("A", "1"))
                                           .addUnit(new Unit("Ax2", "2.0")));

        return categories;
    }
}
