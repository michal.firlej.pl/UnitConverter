package com.michal.data.source;

import com.michal.data.Category;

import java.util.List;

public interface CategoriesSource {
    public List<Category> get();
}
