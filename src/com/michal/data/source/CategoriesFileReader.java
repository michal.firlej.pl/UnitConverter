package com.michal.data.source;

import com.michal.data.Category;

import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;
import java.util.stream.Collectors;

public class CategoriesFileReader implements CategoriesSource {
    private String filePath;

    public CategoriesFileReader(String filePath) {
        this.filePath = filePath;
    }

    @Override
    public List<Category> get() {
        // Java ma pojebane czytanie z plików...
        List<String> lines;
        try {
            lines = Files.lines(Paths.get(filePath))
                         .collect(Collectors.toList());
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

        // TODO: tutaj lecisz po liniach i parsujesz
        lines.forEach(System.out::println);

        return null; // TODO: zaimplementuj pan, użyj this.filePath. (:
    }
}
