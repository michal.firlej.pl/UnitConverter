package com.michal.data;

import java.util.ArrayList;
import java.util.List;

public class Category {
    private String name;
    private List<Unit> units = new ArrayList<>();

    public Category(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public List<Unit> getUnits() {
        return units;
    }

    public Category addUnit(Unit unit) {
        units.add(unit);
        return this; // to nam gwarantuje fluent API, czyli category.add(a).add(b);
    }
}
